## Steps required to import to your android project

1. import blecommj and unattendedsdk aar
2. declare a class implementing interface VendControllerCallback and implements the overriding methods
3. declare a member of class VendController
4. and you are good to go

## 1. Import blecommj and unattendedsdk aar

1. go to the link https://developer.android.com/studio/projects/android-library.html#AddDependency
2. make sure that you import both aar that are provided in this project
  1. blecommj/blecommj-release.aar
  2. unattendedsdk/Unattendedsdk-release.aar

## 2. Declare a class implementing interface VendControllerCallback and implements the overriding methods

Following the class VendingPresenter, implement the interface VendControllerCallback and implements the following methods

1. void connected()
invoked on the event of successfully connected to targeted machine

2. void authRequest()
invoked when there is interaction with the machine that requires funds to proceed to further action

3. void processCompleted()
invoked on the event of vending success | failure | unable to connect | unable to locate machine

4. void disconnected()
invoked on the event of disconnection between the target connected machine and mobile

5. void timeoutWarning()
will be invoke when the targeted connected machine have invoked a timeout signal

6. Activity getActivity()
to provide the current active activity

## 3. Declare a member of class VendController
This is the point of interaction in which the mobile app will use to start connection and send messages.
In VendingPresenter you see the following declaration
'''
this.mVendController = new VendControllerImpl(this);
'''

To start connection, the following parameters are required, the machine serial number and the model number
'''
mVendController.init(mPeripheralSerialNumber, mPeripheralModelNumber, "fff0");
mVendController.connect();
'''

In order to msg the machine that you have finished payment and to proceed to vending,
please get the payload from the validation service and attach it to this method
so that the machine will start vending
'''
mVendController.approveAuth(result.getApprovalPayload());
'''
