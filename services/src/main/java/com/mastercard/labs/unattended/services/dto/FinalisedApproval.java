package com.mastercard.labs.unattended.services.dto;

/**
 * Created by douglas on 10/8/16.
 */
public class FinalisedApproval {
    String id;
    Boolean cancelled;

    public String getId() {
        return id;
    }

    public Boolean getCancelled() {
        return cancelled;
    }
}
