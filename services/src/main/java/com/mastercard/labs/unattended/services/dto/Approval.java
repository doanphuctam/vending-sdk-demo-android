package com.mastercard.labs.unattended.services.dto;

/**
 * Created by douglas on 10/8/16.
 */
public class Approval {
    String id;
    String approvalPayload;

    public String getId() {
        return id;
    }

    public String getApprovalPayload() {
        return approvalPayload;
    }
}
