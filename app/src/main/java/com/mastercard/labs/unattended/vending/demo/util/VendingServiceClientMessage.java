package com.mastercard.labs.unattended.vending.demo.util;

/**
 * @author Muhammad Azeem (muhammad.azeem@mastercard.com)
 */
public enum VendingServiceClientMessage {
    ON_CONNECTED,
    ON_STATS,
    ON_SESSION,
    ON_APPROVE,
    ERR_SESSION_EXPIRED,
    ERR_REQUIRE_CVC,
    ERR_GENERIC,
    APPROVING,
    ON_VEND_SUCCESS,
    ON_VEND_FAILURE,
    ON_VEND_CANCEL,
    ON_TIMEOUT,
    ON_CONNECTION_FAILED,
    ON_DISCONNECTED,
    ON_TIMEOUT_WARN,
    ON_SESSION_END,
    ON_BLUETOOTH_CONNECTED,
    VEND_COMPLETE,
    ALREADY_IN_TRANSACTION,
    CLIENT_REGISTERED,
    CONNECTION_TIMEOUT,
    CONNECTION_TIMER_FINISHED,
    SELECTION_TIMER_VALUE_CHANGED,
    CONNECTION_TIMER_VALUE_CHANGED,
    SELECTION_TIMER_FINISHED;

    public static VendingServiceClientMessage[] values = values();

    public static VendingServiceClientMessage msgFromOrdinal(int ordinal) {
        if (ordinal >= 0 && ordinal < values.length)
            return values[ordinal];
        return null;
    }
}
