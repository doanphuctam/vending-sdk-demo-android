package com.mastercard.labs.unattended.vending.demo.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.mastercard.labs.unattended.vending.demo.R;

/**
 * Created by jameslian on 15/8/16.
 */
public class PrefHelper {
    private static final String TAG = PrefHelper.class.getSimpleName();

    public static String getURL(Context context) throws Exception {

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        String selection = sharedPreferences.getString(context.getString(R.string.pref_key_partner_server), "Custom");
        //return the value of Staging or Production if there are none
        if (!selection.equals("Custom"))
            return selection;

        else {
            boolean isSecured = sharedPreferences.getBoolean(context.getString(R.string.pref_key_partner_server_custom_secure), false);
            String partnerServerHost = sharedPreferences.getString(context.getString(R.string.pref_key_partner_server_custom_host), "");
            String appContext = sharedPreferences.getString(context.getString(R.string.pref_key_partner_server_custom_context), "");

            if (!partnerServerHost.isEmpty())
                selection = (isSecured ? "https://" : "http://") + partnerServerHost;

            if (selection.isEmpty())
                throw new Exception("Invalid Url Setting");

            if(!selection.matches("\\/$"))
                selection += "/";

            if(!appContext.matches("\\/$"))
                appContext += "/";

            selection += appContext;

            return selection;
        }

    }

    public static float[] getLatLog(Context context) {
        float[] returnLocation = new float[2];
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        boolean isManualLocation = sharedPreferences.getBoolean(context.getString(R.string.pref_key_toggle_location), true);
        if (isManualLocation) {
            returnLocation[0] = Float.parseFloat(sharedPreferences.getString(context.getString(R.string.pref_key_latitude), "0.0f"));
            returnLocation[1] = Float.parseFloat(sharedPreferences.getString(context.getString(R.string.pref_key_longitude), "0.0f"));
            return returnLocation;
        }
//        LocationUtil.getFloat


        return null;

    }

}
