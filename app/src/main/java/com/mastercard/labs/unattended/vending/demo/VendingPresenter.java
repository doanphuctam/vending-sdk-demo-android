package com.mastercard.labs.unattended.vending.demo;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

import com.mastercard.labs.unattended.services.PartnerService;
import com.mastercard.labs.unattended.services.dto.Approval;
import com.mastercard.labs.unattended.services.dto.FinalisedApproval;
import com.mastercard.labs.unattended.vending.demo.util.VendBluetoothManager;
import com.mastercard.labs.unattendedsdk.ProcessStatus;
import com.mastercard.labs.unattendedsdk.VendController;
import com.mastercard.labs.unattendedsdk.VendControllerCallback;
import com.mastercard.labs.unattendedsdk.VendControllerImpl;
import com.mastercard.labs.unattendedsdk.VendingMessage;
import com.yosanai.blecommj.BLEScan;

import bolts.Continuation;
import bolts.Task;

/**
 * Created by jameslian on 28/6/16.
 */
class VendingPresenter implements VendControllerCallback, VendBluetoothManager.Callback, VendingContract.UserActionsListener {
    private static final String TAG = VendingPresenter.class.getSimpleName();
    private VendingContract.View mView;
    private MainThread mainThread;
    private VendBluetoothManager mBluetoothManager;
    private boolean enablingBluetooth;
    private BLEScan mBLEScanner;

    private VendController mVendController;

    private String mPeripheralSerialNumber;
    private String mPeripheralModelNumber;

    private String mTransactionId;
    private String mApproveToken;
    private int mVendingAmount;

    public static final String INTENT_KEY_NAME = "INTENT_KEY_NAME";
    public static final String INTENT_KEY_LOCATION = "INTENT_KEY_LOCATION";
    public static final String INTENT_KEY_MODEL = "INTENT_KEY_MODEL";
    public static final String INTENT_KEY_SERIAL = "INTENT_KEY_SERIAL";
    public static final String INTENT_KEY_ADDRESS = "INTENT_KEY_MODEL_ADDRESS";
    public static final String INTENT_KEY_DESCRIPTION = "INTENT_KEY_MODEL_DESCRIPTION";

    VendingPresenter(VendingContract.View view, MainThread mainThread, VendBluetoothManager bluetoothManager) {

        if (view == null) throw new NullPointerException();
        this.mView = view;
        this.mainThread = mainThread;
        this.mBluetoothManager = bluetoothManager;
        this.mVendController = new VendControllerImpl(this);
    }


    @Override
    public void enableBluetooth(boolean permitted) {
        Log.d(TAG, String.format("Enable bluetooth %s", permitted));
        enablingBluetooth = true;
        mBLEScanner.enableBluetoothAdapter();
    }

    @Override
    public void onCreate(Bundle bundle) {

        Log.d(TAG, "onCreate");
        mView.setUserActionsListener(this);

        if (bundle != null) {
            mPeripheralModelNumber = bundle.getString(INTENT_KEY_MODEL);
            mPeripheralSerialNumber = bundle.getString(INTENT_KEY_SERIAL);
        }
    }

    @Override
    public void onResume() {
        Log.d(TAG, "onResume");
//        if (mBluetoothManager.isBluetoothEnabled()) {
//            connectMachine();
//        }
    }

    @Override
    public void onPause() {
        Log.d(TAG, "onPause");
        if (mBLEScanner != null) {
            mBLEScanner.disconnect();
        }
        if (mVendController != null)
            mVendController.disconnect();

    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "onDestroy");
        mBluetoothManager.stop();

    }
    //End VendingContract.UserActionsListener

    // Start VendBluetoothManager.Callback
    @Override
    public void bluetoothTurnedOn() {
        Log.d(TAG, "Bluetooth turned on");

        if (enablingBluetooth) {
            enablingBluetooth = false;
            connectMachine();
        }
    }

    @Override
    public void bluetoothTurnedOff() {
        Log.d(TAG, "Bluetooth turned off");
    }
    // End VendBluetoothManager.Callback

    public void connectMachine() {
        mVendController.init(mPeripheralSerialNumber, mPeripheralModelNumber, "fff0");
        mVendController.connect();
        mView.showConnecting();
    }

    @Override
    public void authRequest(int amount, String payload) {
        Log.d(TAG, "authRequest: ");
        mVendingAmount = amount;
        mApproveToken = payload;
        DemoApplication application = DemoApplication.getApplication((Activity) mView);
        final PartnerService partnerService = getPartnerService();
        partnerService.createApproval(amount, mApproveToken)
                .onSuccess(new Continuation<Approval, Task<Approval>>() {
                    @Override
                    public Task<Approval> then(Task<Approval> task) throws Exception {
                        //Action after successful approval
                        Approval result = task.getResult();
                        mTransactionId = result.getId();
                        mVendController.approveAuth(result.getApprovalPayload());
                        mView.showApproving();
                        return task;
                    }
                });
        mView.showRequestingAuth();
    }

    @Override
    public void connected() {
        mView.showMakeSelection();
    }

    @Override
    public void processCompleted(ProcessStatus status, int amount, String payload, VendingMessage vendingMessage) {
        Log.d(TAG, "processCompleted: ");
        completeSession(status, payload, null);
        if (status == ProcessStatus.SUCCESS)
            mView.showVendingSuccess(amount);
        else if (status == ProcessStatus.INVALID) {
            mView.showErrorMessage(vendingMessage);
        }

    }

    @Override
    public void disconnected() {
    }

    @Override
    public void timeoutWarning() {

    }

    private void completeSession(ProcessStatus status, String payload, Continuation continuation) {
        final PartnerService partnerService = getPartnerService();
        String state = status.getCode().equals(ProcessStatus.SUCCESS.getCode()) ? ProcessStatus.SUCCESS.getCode() : ProcessStatus.FAIL.getCode();
        partnerService.finaliseApproval(mTransactionId, state, payload)
                .onSuccess(new Continuation<FinalisedApproval, Task<FinalisedApproval>>() {
                    @Override
                    public Task<FinalisedApproval> then(Task<FinalisedApproval> task) throws Exception {
                        return task;
                    }
                })
                .continueWith(continuation);
    }


    private PartnerService getPartnerService() {

        DemoApplication application = DemoApplication.getApplication((Activity) mView);
        return application.getPartnerService();
    }

    @Override
    public Activity getActivity() {
        return (Activity) mView;

    }
}