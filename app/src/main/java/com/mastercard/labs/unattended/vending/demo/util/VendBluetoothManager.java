package com.mastercard.labs.unattended.vending.demo.util;

/**
 * @author Muhammad Azeem (muhammad.azeem@mastercard.com)
 */
public interface VendBluetoothManager {
    interface Callback {
        void bluetoothTurnedOn();

        void bluetoothTurnedOff();
    }

    void start(VendBluetoothManager.Callback callback);

    void stop();

    boolean isBluetoothEnabled();
}
