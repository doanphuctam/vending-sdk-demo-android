package com.mastercard.labs.unattended.vending.demo;

/**
 * Created by jameslian on 1/9/16.
 */
public class Amount {
    int amountMinorUnits;

    public int getAmountMinorUnits() {
        return amountMinorUnits;
    }

    public void setAmountMinorUnits(int amountMinorUnits) {
        this.amountMinorUnits = amountMinorUnits;
    }
}
