package com.mastercard.labs.unattended.vending.demo.util;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.util.Log;

/**
 * @author Muhammad Azeem (muhammad.azeem@mastercard.com)
 */
public class VendBluetoothManagerImpl implements VendBluetoothManager {
    private static final String TAG = VendBluetoothManagerImpl.class.getName();

    private Context context;
    private BluetoothAdapterReceiver bluetoothAdapterReceiver;
    private boolean started;

    public VendBluetoothManagerImpl(Context context) {
        this.context = context;
    }

    @Override
    public void start(VendBluetoothManager.Callback callback) {
        if (started)
            return;

        bluetoothAdapterReceiver = new BluetoothAdapterReceiver(callback);

        try {
            if (context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
                // Register for broadcasts on BluetoothAdapter state change
                IntentFilter filter = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
                context.registerReceiver(bluetoothAdapterReceiver, filter);
                started = true;
            }
        } catch (Exception e) {
            Log.e(TAG, "BLE Exception:", e);
        }
    }

    @Override
    public void stop() {
        if (started) {
            context.unregisterReceiver(bluetoothAdapterReceiver);
            started = false;
        }
    }

    @Override
    public boolean isBluetoothEnabled() {
        final BluetoothManager bluetoothManager = (BluetoothManager) context.getSystemService(Context.BLUETOOTH_SERVICE);
        BluetoothAdapter bluetoothAdapter = bluetoothManager.getAdapter();
        return bluetoothAdapter != null && bluetoothAdapter.isEnabled();
    }
}
