package com.mastercard.labs.unattended.vending.demo;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.ArrayAdapter;

/**
 * Created by jameslian on 16/8/16.
 */
public class DrawerListAdapter extends ArrayAdapter<String> implements SharedPreferences.OnSharedPreferenceChangeListener {
    private static final String TAG = "DrawerListAdapter";

    public static final String ITEM_LOGOUT = "Logout";
    public static final String ITEM_SETTINGS = "Settings";
    public static final String ITEM_QKR = "QkR";

    private final String[] items;
    private boolean user_logged;

    public DrawerListAdapter(Context context, String[] items) {
        super(context, R.layout.drawer_list_item, R.id.drawer_item);
        DemoApplication demoAppl = DemoApplication.getApplication((Activity) this.getContext());
        user_logged = demoAppl.isUserLoggedIn();
        demoAppl.getPreferences().registerOnSharedPreferenceChangeListener(this);
        this.items = items;
    }

    @Override
    public int getCount() {
        return items.length;
    }

    @Override
    public String getItem(int position) {
        return items[position];
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s) {
        Log.d(TAG, "onSharedPreferenceChanged: User Logged:" + user_logged);
        DemoApplication demoAppl = DemoApplication.getApplication((Activity) this.getContext());
        user_logged = demoAppl.isUserLoggedIn();
        this.notifyDataSetChanged();
    }
}
